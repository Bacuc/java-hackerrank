import java.math.BigDecimal;
import java.util.*;
class Solution{

    public static void main(String []args){
        //Input
        Scanner sc= new Scanner(System.in);
        int n=sc.nextInt();
        String []s=new String[n+2];
        for(int i=0;i<n;i++){
            s[i]=sc.next();
        }
      	sc.close();
        //Write your code here
        BigDecimal a;
        BigDecimal b;
        int index = 0;
        while(index < s.length -3)
        {
            a = new BigDecimal(s[index]);
            b = new BigDecimal(s[index+1]);
            if(a.compareTo(b) < 0)
            {
                String temp = s[index+1];
                s[index+1] = s[index];
                s[index] = temp;
                index = 0;
            }
            else
            {
                index++;
            }
        }

        //Output
        for(int i=0;i<n;i++)
        {
            System.out.println(s[i]);
        }
    }

}