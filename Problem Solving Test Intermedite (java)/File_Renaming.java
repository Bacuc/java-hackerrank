import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class solution {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String newName = scanner.nextLine();
        String oldName = scanner.nextLine();

        int result = renameFile(newName, oldName);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();
        scanner.close();
    }

    public static int renameFile(String newName, String oldName) {
        int n = newName.length();
        int m = oldName.length();
        int[] dp = new int[m + 1];
        for (int i = 1; i <= n; i++) {
            int[] dpp = new int[m + 1];
            for (int j = i; j <= m; j++) {
                dpp[j] = dpp[j - 1];
                if (newName.charAt(i - 1) == oldName.charAt(j - 1)) {
                    dpp[j] += dp[j - 1];
                }
            }
            dp = dpp;
        }
        return dp[m] % (int) (Math.pow(10, 9) + 7);
    }
}