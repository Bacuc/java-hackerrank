import java.util.Scanner;

public class solution {
    public static long maxSubarrayValue(int arr_count, int[] arr) {
        int i, j, k;
        long ans = 0, a, b;
        long[] even = new long[arr_count+1];
        long[] odd = new long[arr_count+1];
        even[0] = 0;
        odd[0] = 0;
        for (i = 0; i < arr_count; i++) {
            if (i % 2 == 0) {
                even[i+1] = even[i] + arr[i];
                odd[i+1] = odd[i];
            } else {
                even[i+1] = even[i];
                odd[i+1] = odd[i] + arr[i];
            }
        }
        for (i = 0; i < arr_count; i++) {
            for (j = i + 1; j <= arr_count; j++) {
                a = even[j] - even[i];
                b = odd[j] - odd[i];
                ans = (a - b)*(a - b) > ans ? (a - b)*(a - b) : ans;
            }
        }
        return ans;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arr_count = scanner.nextInt();
        int[] arr = new int[arr_count];
        for (int i = 0; i < arr_count; i++) {
            arr[i] = scanner.nextInt();
        }
        long result = maxSubarrayValue(arr_count, arr);
        System.out.println(result);
    }
}