import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result {

    /*
     * Complete the 'decryptPassword' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static String decryptPassword(String s) {
    // Write your code here
        char[] sArray = s.toCharArray();
        int i = 0;
         while (i < sArray.length && Character.isDigit(sArray[i]) && sArray[i] != '0') {
        i++;
        }
        List<Integer> zeroIndexes = new ArrayList<>();
            for (int l = i; l < sArray.length; l++) {
                if (sArray[l] == '0') {
                    zeroIndexes.add(l);
                }
            }
            for (int j = 0; j < zeroIndexes.size(); j++) {
                sArray[zeroIndexes.get(j)] = sArray[i - j - 1];
            }
            for (int j = i; j < sArray.length; j++) {
                if (sArray[j] == '*') {
                    char temp = sArray[j - 1];
                    sArray[j - 1] = sArray[j - 2];
                     sArray[j - 2] = temp;
            }
    }
    return new String(sArray, i, sArray.length - i).replace("*", "");
}
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = bufferedReader.readLine();

        String result = Result.decryptPassword(s);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
