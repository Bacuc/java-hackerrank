/*
Given a time in -hour AM/PM format, convert it to military (24-hour) time.

Note: - 12:00:00AM on a 12-hour clock is 00:00:00 on a 24-hour clock.
- 12:00:00PM on a 12-hour clock is 12:00:00 on a 24-hour clock. 
*/
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        System.out.println(timeConversion(s));
    }

    public static String timeConversion(String s) {
        int hour = Integer.parseInt(s.substring(0, 2));
        String ampm = s.substring(8);
        if (ampm.equals("AM")) {
            if (hour == 12) {
                return "00" + s.substring(2, 8);
            }
            return s.substring(0, 8);
        } else {
            if (hour == 12) {
                return s.substring(0, 8);
            }
            return (hour + 12) + s.substring(2, 8);
        }
    }
}
