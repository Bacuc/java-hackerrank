/*
Implement a simple text editor. The editor initially contains an empty string,s. Perform q operations of the following 4 types:
1.append(w) - Append string  to the end of .
2.delete(k) - Delete the last  characters of .
3.print (k)- Print the  character of .
4.undo() - Undo the last (not previously undone) operation of type  or , reverting  to the state it was in prior to that operation.
...
...
...

*/
import java.io.*;
import java.util.*;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        long S = sc.nextInt();
        int tag,k;
        String last, newString;
        Stack<String>stack = new Stack<>();
        
        while(S-->0){
            tag = sc.nextInt();
            switch(tag){
                case 1: 
                //append(W)
                last = stack.size() > 0 ? stack.peek(): "";
                newString = last + sc.next();
                //System.out.println(tag);
                stack.push(newString);
                break;
                
                case 2 :
                //Print the k characters of s
                k = sc.nextInt();
                last = stack.peek();
                newString = last.substring(0, last.length()-k);
                //System.out.println(tag + " " + newString);
                stack.push(newString);
                break;
                
                case 3:
                    //return kth character of S
                    k = sc.nextInt()-1;
                    if(stack.size() > 0) {
                        last = stack.peek();
                        String c = String.valueOf(last.charAt(k));
                        //System.out.println(tag + " " + c);
                        System.out.println(c);
                    }
                    break;
                case 4:
                    //undo
                    //System.out.println(tag + " " + stack.peek());
                    stack.pop();
                    break;
            }
        }
    }
}
