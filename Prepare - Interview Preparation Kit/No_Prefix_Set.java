import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'noPrefix' function below.
     *
     * The function accepts STRING_ARRAY words as parameter.
     */

  public static void noPrefix(List<String> words) {
    // Write your code here

        HashSet<String> hsWords = new HashSet<>();
        HashSet<String> hsPrefix = new HashSet<>();
        
        for (String word : words) { // O(n)
            // if the word matches any of the prior prefixes
            if (hsPrefix.contains(word)) {
                    System.out.println("BAD SET");
                    System.out.println(word);
                    return;
            }
            
            // for all of the possible prefixes of current word
            for (int i = 1; i <= word.length(); ++i) { // O(word.length) < 60
                String sub = word.substring(0, i);
                // if any of the previous words are a prefix to this word
                if (hsWords.contains(sub)) {
                    System.out.println("BAD SET");
                    System.out.println(word);
                    return;
                }
                // we add all the prefixes to the prefix hashset
                if (i != word.length()) hsPrefix.add(sub);
            }
            // we add the word to the word hashset
            hsWords.add(word);
        }
        System.out.println("GOOD SET");
        return;
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> words = IntStream.range(0, n).mapToObj(i -> {
            try {
                return bufferedReader.readLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
            .collect(toList());

        Result.noPrefix(words);

        bufferedReader.close();
    }
}
