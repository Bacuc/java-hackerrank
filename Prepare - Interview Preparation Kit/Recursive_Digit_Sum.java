/*
We define super digit of an integer x using the following rules:
Given an integer, we need to find the super digit of the integer.
If  has only  digit, then its super digit is x .
Otherwise, the super digit of  is equal to the super digit of the sum of the digits of .
For example, the super digit of  will be calculated as:
super_digit(9875)       9+8+7+5 = 29 
    super_digit(29)     2 + 9 = 11
    super_digit(11)        1 + 1 = 2
    super_digit(2)        = 2  
...
...
...
*/
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.Scanner;



public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Solution s = new Solution();
        String str_n = sc.next();
        int k = sc.nextInt();
        
        int pSum = Integer.parseInt(s.supdig(str_n));
        pSum *= k;
        String sup = Integer.toString(s.supdig(pSum));
        System.out.println(sup);
     }
    String supdig(String n) {
        if(n.length() == 1) 
            return n;
        else {
            int np = 0;
            
        for(int i = 0; i < n.length(); i++) {
                np += Character.getNumericValue( n.charAt(i) );    
        }
            
            return supdig(Integer.toString(np));
        }       
    }
    
    int supdig(int n) {
        if(n / 10 == 0) 
            return n;
        else {
            int r = 0;
            
            while(n > 0) {
                r += n % 10;
                n /= 10;
            }
            
            return supdig(r);
        }
    }

}
