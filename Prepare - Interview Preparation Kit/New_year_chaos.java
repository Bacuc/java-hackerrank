/*
It is New Year's Day and people are in line for the Wonderland rollercoaster ride. Each person wears a sticker indicating their initial position in the queue from 1 to n . Any person can bribe the person directly in front of them to swap positions, but they still wear their original sticker. One person can bribe at most two others.

Determine the minimum number of bribes that took place to get to a given queue order. Print the number of bribes, or, if anyone has bribed more than two people, print Too chaotic.

Example
q = [1,2,3,4,5,6,7,8]
If person 5 bribes person 4, the queue will look like this:1,2,3,4,5,6,7,8 . Only 1 bribe is required. Print 1.
q = [4,1,2,3]
Person 4 had to bribe 3 people to get to the current position. Print Too chaotic.
Function Description
Complete the function minimumBribes in the editor below.
minimumBribes has the following parameter(s):
int q[n]: the positions of the people after all bribes
Returns
No value is returned. Print the minimum number of bribes necessary or Too chaotic if someone has bribed more than 2 people.
*/
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner s = new Scanner(System.in);
        int T = s.nextInt();
        int[] nn = new int[100000];
        
        for(int t=0;t<T;t++)
        {
            int n = s.nextInt();
            
            int x, y=0;
            
            for(int j=0;j<n;j++)
            {
                nn[j] = s.nextInt();
                
                if(nn[j]-j > 3)
                {
                    y = -1;
                }
            }
            
            if(y == -1)
            {
                System.out.println("Too chaotic");
                continue;
            }
            
            int yr;
            for(int k=0;k<n;k++)
            {
                yr = y;
                for(int j=0;j<n-1;j++)
                {
                    if(nn[j] > nn[j+1])
                    {
                        x = nn[j];
                        nn[j] = nn[j+1];
                        nn[j+1] = x;
                        y++;
                    }
                }
                if(yr == y)
                    break;
            }
            
            System.out.println(y);
            
        }
    }
}