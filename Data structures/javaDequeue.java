/*
In computer science, a double-ended queue (dequeue, often abbreviated to deque, pronounced deck) 
is an abstract data type that generalizes a queue, for which elements can be added to or removed from either 
the front (head) or back (tail).
Deque interfaces can be implemented using various types of collections such as LinkedList or ArrayDeque classes.
For example, deque can be declared as: 
Deque deque = new LinkedList<>();
or
Deque deque = new ArrayDeque<>();
You can find more details about Deque here.

In this problem, you are given N integers. You need to find the maximum number of unique integers among all
the possible contiguous subarrays of size M.

Note: Time limit is  second for this problem.
Input Format

The first line of input contains two integers N and M : representing the total number of 
integers and the size of the subarray, respectively. The next line contains N space separated integers.
Constraints
1<= N <=100000
1<= M <= 100000
M<=N
The number in the array will range between[0,20000000].
*/

    import java.util.*;
    public class test {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            Deque deque = new ArrayDeque<>();
            HashSet<Integer> set = new HashSet<>();
            int n = in.nextInt();
            int m = in.nextInt();
            int max = Integer.MIN_VALUE;

            for (int i = 0; i < n; i++) {
                int num = in.nextInt();
               
                deque.add(num);
                set.add(num);
                
                if(deque.size() == m){
                    if(set.size() > max)
                        max = set.size();
                    
                    int first = (int)deque.remove();
                    if(!deque.contains(first))
                        set.remove(first);
                }
                
            }
            System.out.println(max);
        }
    }



