import java.io.*;
import java.util.*;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        List<Integer> ls = new ArrayList<Integer>();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for(int i = 0; i < n; i++)
        {
            ls.add(sc.nextInt());
        }
        int q = sc.nextInt();
        for(int i = 0; i < q; i++)
        {
            String ins = sc.next();
            if(ins.compareTo("Insert") == 0)
            {
                int x = sc.nextInt();
                int pos = sc.nextInt();
                ls.add(x,pos);
            }
            else if(ins.compareTo("Delete") == 0)
            {
                int pos = sc.nextInt();
                ls.remove(pos);
            }
            else
            {
                System.out.println("somethin bad happen");
            }
        }
        for(int lis: ls)
        {
            System.out.print(lis+" ");
        }
        sc.close();
    }
}
 
